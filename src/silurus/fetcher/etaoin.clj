(ns silurus.fetcher.etaoin
  (:require [etaoin.api :as e]
            [babashka.process.pprint]))

(def default-driver-options {})

;; (def driver (e/chrome))

(defn start [config]
  (let [driver-config (::driver config)
        init (::init config)
        driver (e/boot-driver (keyword (:type driver-config)) (merge default-driver-options driver-config))]
    (if init (do (init) driver) driver)))

(defn make-ident [_ {:keys [url]}]
  (if url
    url
    (throw (ex-info "Cannot infer ident for etaoin" {}))))

(def html-snapshot
  {:name :html-snapshot
   :leave (fn leave-html-snapshot [ctx]
            (let [driver (get-in ctx [:components ::driver])
                  html (e/get-source driver)]
              (-> ctx
                  (assoc-in [:act-node :result :body] html)
                  (assoc-in [:act-node :result :headers "content-type"] "text/html"))))})

(defn request [{:keys [components config] :as env}
               {:keys [url action export-html?] :as node}]
  (let [driver (::driver components (start config))
        new-env (assoc-in env [:components ::driver] driver)]
    (if url
      (e/go driver url)
      (action new-env node))
    [new-env (if export-html?
               (-> node
                   (assoc-in [:result :body] (e/get-source driver))
                   (assoc-in [:result :headers "content-type"] "text/html"))
               node)]))

(defn driver [{:keys [components config] :as env}]
  (let [driver (::driver components (start config))
        new-env (assoc-in env [:components ::driver] driver)]
    [driver new-env]))

;; {:response {:sessionId "4c745abaf6c4ad22e43a7ef2d58fb701", :status 13, :value {:message "unknown error: session deleted because of page crash\nfrom unknown error: cannot determine loading status\nfrom tab crashed\n  (Session info: chrome=107.0.5304.68)\n  (Driver info: chromedriver=107.0.5304.62 (1eec40d3a5764881c92085aaee66d25075c159aa-refs/branch-heads/5304@{#942}),platform=Linux 6.0.2-arch1-1 x86_64)"}}, :path "session/4c745abaf6c4ad22e43a7ef2d58fb701/element/0.8988512690748953-9292/click", :payload nil, :method :post, :type :etaoin/http-error, :port 38845, :host "127.0.0.1", :status 200, :webdriver-url nil, :driver {:args ("chromedriver" "--port=38845"), :capabilities {:loggingPrefs {:browser "ALL"}}, :process {:proc #object[java.lang.ProcessImpl 0x11e66742 "Process[pid=1025649, exitValue=\"not exited\"]"], :exit nil, :in #object[java.lang.ProcessImpl$ProcessPipeOutputStream 0x3207e958 "java.lang.ProcessImpl$ProcessPipeOutputStream@3207e958"], :out #object[java.lang.ProcessBuilder$NullInputStream 0x63bfd77e "java.lang.ProcessBuilder$NullInputStream@63bfd77e"], :err #object[java.lang.ProcessBuilder$NullInputStream 0x63bfd77e "java.lang.ProcessBuilder$NullInputStream@63bfd77e"], :prev nil, :cmd ["chromedriver" "--port=38845"]}, :locator "xpath", :type :chrome, :port 38845, :host "127.0.0.1", :url "http://127.0.0.1:38845", :session "4c745abaf6c4ad22e43a7ef2d58fb701"}}
